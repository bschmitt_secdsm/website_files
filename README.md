## How to contribute:  
1. create a fork
1. Make changes to data.json  
1. create pull request against pilot  
* once approved - changes will be deployed to [https://pilot.secdsm.org/](https://pilot.secdsm.org)  

###  Merge Access to Pilot
The idea is that you should be able to approve your own merge requests to pilot.secdsm.org  
Hit up zoomequipd in slack for merge access to pilot

### Deploying to secdsm.org
when changes are ready for "prod" simply create a pull request to merge pilot to master - once approve the changes will be pushed to [https://secdsm.org](https://secdsm.org)


---------------------------------------


# data.json details
Must be a json formatted file - this controls all the dynamic fields for the [secdsm.org](https://secdsm.org) page  
There are two main sections to the data.json file

  1. [schedules](https://bitbucket.org/secdsm/website_files/overview#markdown-header-schedules)
  1. [events](https://bitbucket.org/secdsm/website_files/overview#markdown-header-events)

## Schedules
an array of hashes containing the details for the schedule section of the website. 

* Each element in the array should contain a full month's worth of details.
  * this includes an ["intro"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-intro-talks), ["tooltalks"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-tool-talks)", and ["features"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-feature-talks) elements- see below for details on those keys.

### Keys Available to the schedule
* "alert"
* "date"
* ["intro"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-intro-talks) - a hash containing the intro talk details
* ["tooltalks"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-tool-talks) - an array containing tools talks
* ["features"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-feature-talks) - an array containing feature talks

##### Intro Talks
###### Keys Available to elements in introtalks array
  * "title"
  * "time"
  * "description"

##### Tool Talks
###### Keys Available to elements in tooltalks array
  * "title"
  * "description" - required
  * "time"
  * "speaker"  - an array containing speaker details
    * "name"
    * "bio" - optional

##### Feature Talks
###### Keys Available to elements in features arrays
  * "title"
  * "description" - optional, but generally desired
  * "time"
  * "speaker"  - an array containing speaker details
    * "name"
    * "bio" - optional

## Events
an array of hashes containing the details for the events section of the website.

* Each element in the array should contain a full month's worth of events.  
* Each element in the "events" array becomes a collapsible box 
* Order matters when adding months to the events section.  
  * The first element will be the element that is expanded.
  * Months are ascending

### Keys Available to elements in events arrays
  * "url"
  * "name"
  * "time"
  * "description" - optional
  * "geolocation"  - optional  
    * You can embed a google maps by :  
    
        1. Go to http://maps.google.com  
        1. Select a point of interest  
        1. Click share  
        1. Click the `Embed map` tab  
        1. Copy the link in the `src` attribute  