#!/usr/bin/env ruby

require 'json'

data_file = File.read('data.json')
p "Attempting Parse"
begin
 JSON.parse(data_file);
 p "Parse Succeeded"
rescue JSON::ParserError => e
    p "Parse Failed: #{e.message[0..200]}..."
end
